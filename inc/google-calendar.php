<?php
/**
 * User: aldo-f
 * Date: 22/11/2018
 * Time: 9:02
 */

$content = file_get_contents('https://calendar.google.com/calendar/embed?src=classroom109473165059831446618%40group.calendar.google.com&amp;ctz=Europe%2FBrussels');
$content = str_replace('</title>', '</title><base href="https://www.google.com/calendar/" />', $content);

/* Change this to work on your site */
$link_to_server = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
$url_calendar = "/google-calendar-styling";
$stylesheet = "/css/google-calendar.css";
$link_to_stylesheet = $link_to_server . $url_calendar . $stylesheet;

$content = str_replace('</head>', '<link rel="stylesheet" href="' . $link_to_stylesheet . '" /></head>', $content);
echo $content;